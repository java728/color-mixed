# Color-Mixed



## Indicaciones

El objetivo de este sistema es permitir al usuario crear efectos de sombras e iluminación sobre algún color en específico. El usuario dispone de un catálogo de muestra de colores (que son cargados desde el archivo CSV), o también, podría ingresar los valores correspondientes del modelo RGB (Red Green Blue) o su valor hexadecimal.

