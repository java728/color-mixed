/******************************************************************************
 *  Compilation:  javac COLOR_MIXED.java
 *  Execution:    java COLOR_MIXED
 *  Barroso Garcia Jhonatan
 *  Ingenieria en Sistemas Computacionales 
 ******************************************************************************/

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;
public class Proyecto extends JFrame 
{
    //Solicitar el valor en Hexadecimal
    private JTextField numHex;
    //Mostrar Los valores númericos de cada color
    private JTextField numRed;
    private JTextField numGreen;
    private JTextField numBlue;
    private JTextField numeroH;
    //Valores para la mezcla de colores
    private JSpinner rojo;
    private JSpinner verde;
    private JSpinner azul;
    //Paneles a utilizar
    private JPanel[] panel;
    //Colores de Muestra
    private JComboBox coloresM;
    //Iluminacion y Sombras
    private JRadioButton normal;
    private JRadioButton sombra;
    private JRadioButton iluminar;
    private ButtonGroup grupo;
    //Color Seleccionado
    private Color miColor;
    private ModeloDatos modelo;
    //JButton Limpiar
    private JButton limpiar;
    public Proyecto(){
        setSize(1060,550);
        miColor=Color.WHITE;
        modelo=new ModeloDatos();
        panel= new JPanel[10];
        for (int i=0;i<panel.length;i++){
            panel[i]= new JPanel();
            panel[i].setBackground(miColor);
        }
        init();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //this.setResizable(false);
        this.setLocation(200,100);
        setVisible(true);
    }

    public void init(){
        //Encabezado
        JLabel enc= new JLabel("MODELO DE COLORES RGB",JLabel.CENTER);
        enc.setBackground(new Color(74,0,140));
        enc.setForeground(Color.WHITE);
        enc.setOpaque(true);
        enc.setFont(new Font("Times New Roman",1,26));

        //Parte lateral izquierda del formulario
        JPanel contenedor= new JPanel(new GridLayout(16,1));
        JPanel p= new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel p1= new JPanel(new FlowLayout());
        JPanel p2= new JPanel(new FlowLayout());
        JPanel p3= new JPanel(new FlowLayout());
        JPanel p4= new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel l1= new JLabel("Seleccionar Color: ",JLabel.LEFT);
        l1.setForeground(new Color(51,0,153));
        l1.setFont(new Font("Times New Roman",1,19));
        coloresM= new JComboBox(modelo.getNombreColores());
        coloresM.setBackground(Color.pink);
        JPanel modelo = new JPanel();
        modelo.add(l1);
        modelo.add(coloresM);
        p.add(new JLabel(""));
        JLabel r= new JLabel(" RED    : ");
        r.setFont(new Font("Times New Roman",1,14));
        r.setForeground(Color.WHITE);
        r.setOpaque(true);
        r.setBackground(Color.red);
        p1.add(r);
        rojo=new JSpinner(new SpinnerNumberModel(0, 0, 255, 1));
        p1.add(rojo);

        JLabel g= new JLabel(" GREEN: ");
        g.setFont(new Font("Times New Roman",1,14));
        g.setOpaque(true);
        g.setBackground(Color.GREEN);
        p2.add(g);
        verde=new JSpinner(new SpinnerNumberModel(0, 0, 255, 1));
        p2.add(verde);

        JLabel a= new JLabel(" BLUE   : ");
        a.setFont(new Font("Times New Roman",1,14));
        a.setForeground(Color.WHITE);
        a.setOpaque(true);
        a.setBackground(Color.BLUE);
        p3.add(a);
        azul=new JSpinner(new SpinnerNumberModel(0, 0, 255, 1));
        p3.add(azul);
        //Valor en hexadecimal
        numHex=new JTextField(9);
        p4.add(numHex);
        contenedor.add(p);
        contenedor.add(new JLabel());

        JLabel tit=new JLabel("Ingrese los valores RGB del color:",JLabel.CENTER);
        tit.setFont(new Font("Times New Roman",1,17));
        tit.setForeground(new Color(0,38,84));

        contenedor.add(tit);
        contenedor.add(p1);
        contenedor.add(p2);
        contenedor.add(p3);
        contenedor.add(new JLabel());
        
        JLabel tit2=new JLabel("Ó el Valor en Hexadecimal:",JLabel.CENTER);
        tit2.setForeground(new Color(0,102,102));
        tit2.setFont(new Font("Tines New Roman",1,17));
        contenedor.add(tit2);
        contenedor.add(p4);
             
        
        //Boton Limpiar 
        contenedor.add(new JLabel());
        limpiar= new JButton("Limpiar Todo");
        limpiar.setBackground(new Color(252,226,22));
        limpiar.setFont(new Font("Times New Roman",1,13));
        contenedor.add(new JLabel());
        JPanel boton= new JPanel();
        boton.add(limpiar);
        contenedor.add(boton);
        //Paneles para colorear el centro
        JPanel centro= new JPanel(new BorderLayout());
        JPanel c= new JPanel(new GridLayout(1,10));

        for (int i=0;i<panel.length;i++)
            c.add(panel[i]);

        centro.add(c,BorderLayout.CENTER);
        //Tintas y Sombras
        JPanel efecto= new JPanel();
        normal=new JRadioButton("Color sin Efecto");
        normal.setSelected(true);
        sombra= new JRadioButton("Sombras");
        iluminar=new JRadioButton("Iluminación");
        grupo= new ButtonGroup();
        grupo.add(normal);
        grupo.add(sombra);
        grupo.add(iluminar);
        normal.setFont(new Font("Times New Roman",1,15));
        sombra.setFont(new Font("Times New Roman",1,15));
        iluminar.setFont(new Font("Times New Roman",1,15));
        efecto.add(sombra);
        efecto.add(new JLabel(" "));
        efecto.add(normal);
        efecto.add(new JLabel(" "));
        efecto.add(iluminar);
        centro.add(efecto,BorderLayout.SOUTH);
        centro.add(new JLabel(" "),BorderLayout.NORTH);

        //Parte Lateral del Formulario (EAST)
        JPanel panelL= new JPanel(new GridLayout(8,1));
        JPanel c1= new JPanel();
        JPanel c2= new JPanel();
        JPanel c3= new JPanel();
        JPanel c4= new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel info= new JLabel("Información sobre el Color Seleccionado",JLabel.CENTER);
        info.setFont(new Font("Times New Roman",1,19));
        info.setForeground(new Color(0,79,66));
        panelL.add(new JLabel());
        
        JLabel infoR= new JLabel("R: ");
        infoR.setFont(new Font("Times New Roman",1,18));
        infoR.setOpaque(true);
        infoR.setForeground(Color.WHITE);
        infoR.setBackground(Color.RED);
        numRed= new JTextField(10);
        numRed.setEditable(false);
        c1.add(infoR);
        c1.add(numRed);
        
        JLabel infoG= new JLabel("G: ");
        infoG.setFont(new Font("Times New Roman",1,18));
        infoG.setOpaque(true);
        infoG.setBackground(Color.GREEN);
        numGreen= new JTextField(10);
        numGreen.setEditable(false);
        c2.add(infoG);
        c2.add(numGreen);
        
        JLabel infoB= new JLabel("B: ");
        infoB.setFont(new Font("Times New Roman",1,18));
        infoB.setOpaque(true);
        infoB.setForeground(Color.WHITE);
        infoB.setBackground(Color.BLUE);
        numBlue= new JTextField(10);
        numBlue.setEditable(false);
        c3.add(infoB);
        c3.add(numBlue);
        
        JLabel infoH= new JLabel(" Hexadecimal: ");
        infoH.setFont(new Font("Times New Roman",1,18));
        infoH.setForeground(new Color(42,88,213));
        numeroH= new JTextField(10);
        numeroH.setEditable(false);
        c4.add(infoH);
        c4.add(numeroH);
        
        panelL.add(modelo);
        panelL.add(info);
        panelL.add(c1);
        panelL.add(c2);
        panelL.add(c3);
        panelL.add(new JLabel(""));
        panelL.add(c4);

        //Agregar Componentes al formulario
        this.add(enc,BorderLayout.NORTH);
        this.add(contenedor,BorderLayout.WEST);
        this.add(centro,BorderLayout.CENTER);
        this.add(panelL,BorderLayout.EAST);
        //Manejo de Eventos
        Eventos e= new Eventos();
        rojo.addChangeListener(e);
        verde.addChangeListener(e);
        azul.addChangeListener(e);
        rojo.addKeyListener(e);
        verde.addKeyListener(e);
        azul.addKeyListener(e);
        numHex.addKeyListener(e);
        coloresM.addItemListener(e);
        limpiar.addActionListener(e);
        //Sombras e Iluminación
        normal.addActionListener(e);
        sombra.addActionListener(e);
        iluminar.addActionListener(e);
    }
     public void imprimirMensaje(String mensaje){
        JOptionPane.showMessageDialog(null,mensaje);
    }
    //Paneles que se pintan en el centro con el fondo del color deseado
    public void limpiarPaneles(){
        for (int i=0;i<panel.length;i++)
            panel[i].setBackground(Color.WHITE);
        
    }

    /****CLASES INTERNAS PARA EL MANEJO DE EVENTOS***/
    public class Eventos implements KeyListener, ChangeListener, ItemListener,ActionListener{
    
     @Override
    public void actionPerformed(ActionEvent e){
        Color blanco=Color.WHITE;
        Color negro=Color.BLACK;
        double valores[]={0.111,0.222,0.333,0.444,0.555,0.666,0.777,0.888,0.944,1.0};
        
        if(e.getSource()==limpiar)
        limpiarTodo();
        else{
            JRadioButton fuente=(JRadioButton)e.getSource();
            
            if(fuente==normal)
              for (int i=0;i<panel.length;i++)
              panel[i].setBackground(miColor);
            else
             if(fuente==sombra)
              for (int i=0;i<panel.length;i++){
                   Color c=modelo.combine(miColor,negro,valores[i]);
                   panel[i].setBackground(c);
               }
               else 
               for (int i=0;i<panel.length;i++){
                   Color c=modelo.combine(miColor,blanco,valores[i]);
                   panel[i].setBackground(c);
               }

        }
    }
        
    @Override
    public void keyTyped(KeyEvent e){}

    @Override
    public void keyPressed(KeyEvent e) {
        Color c= null;
        if(e.getKeyChar()==e.VK_ENTER)
        if(e.getSource() instanceof JSpinner){
            normal.setSelected(true);
            int r=(int)rojo.getValue();
            int g=(int)verde.getValue();
            int b=(int)azul.getValue();
            miColor= new Color(r,g,b);
            actualizarEtiquetas(miColor);
             numeroH.setText(modelo.getHexadecimal(miColor));
            for(int x=0;x<panel.length;x++)
            panel[x].setBackground(miColor);
        }else {
            normal.setSelected(true);
            String val=reemplazarAcentos(numHex.getText());
            if(validarNumeroHex(val)){
              miColor=new Color(modelo.hex(val));
              actualizarEtiquetas(miColor);
              numeroH.setText(modelo.getHexadecimal(miColor));
              for(int x=0;x<panel.length;x++)
              panel[x].setBackground(miColor);
            }
            
            else {
                imprimirMensaje("Ingrese un valor valido!!!!");
                numHex.setText("");
                limpiarTodo();
            }
        }
        
    }
    public String reemplazarAcentos(String s){
        s.toUpperCase();
        String val="";
        for (int i=0;i<s.length();i++){
            char letra=s.charAt(i);
           if(letra=='á'||letra=='Á')val=val+"a";else
               if(letra=='é' || letra=='É')val=val+"e";
               else val=val+letra;
           
        }
        numHex.setText(val);
        return val;
    }
    public boolean validarNumeroHex(String num){
        boolean flag=true;
        char []dig={'a','A','b','B','c','C','d','D','e','E','f','F'};
        if(num.charAt(0)=='#' && num.length()==7){
            for (int i=1;i<num.length();i++){
            char letra=num.charAt(i);
            if(Character.isDigit(letra)||letra=='a' || letra=='A'|| letra=='b'|| letra=='B' || letra=='c' || letra=='C' || letra=='d' || letra=='D' || letra=='e'|| letra=='E' || letra=='f'| letra=='F'){
             }else{
                 flag=false;
                 break;
            }

           } 
        }else flag=false;
        return flag;
    }
    
    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void stateChanged(ChangeEvent e) {
        normal.setSelected(true);
        int r=(int)rojo.getValue();
        int g=(int)verde.getValue();
        int b=(int)azul.getValue();
        miColor=new Color(r,g,b);
        numeroH.setText(modelo.getHexadecimal(miColor));
        for(int x=0;x<panel.length;x++)
            panel[x].setBackground(miColor);
    
    }
    public void actualizarEtiquetas(Color c){
        //Actualizar los valores de los Campos de Texto de los colores
        int num[]=modelo.getCantidadColores(c);
        numRed.setText(" "+num[0]);
        numGreen.setText(" "+num[1]);
        numBlue.setText(" "+num[2]);
    }
    @Override
    public void itemStateChanged(ItemEvent e) {
        JComboBox fuente=(JComboBox)e.getSource();
        normal.setSelected(true);
        if(fuente.getSelectedIndex()>=1){
        String nomColor=(String)coloresM.getSelectedItem();
        miColor=modelo.buscarColor(nomColor);
         actualizarEtiquetas(miColor);
         numeroH.setText(modelo.getHexadecimal(miColor));
        for(int x=0;x<panel.length;x++)
            panel[x].setBackground(miColor);
        
       }else limpiarE();
     }
    public void limpiarE(){
        numRed.setText("");
        numGreen.setText("");
        numBlue.setText("");
        numeroH.setText("");
        limpiarPaneles();
        numHex.setText("");
        rojo.setModel(new SpinnerNumberModel(0, 0, 255, 1));
        verde.setModel(new SpinnerNumberModel(0, 0, 255, 1));
        azul.setModel(new SpinnerNumberModel(0, 0, 255, 1));
        miColor=Color.WHITE;
        normal.setSelected(true);
     }
    public void limpiarTodo(){
        numRed.setText("");
        numGreen.setText("");
        numBlue.setText("");
        numeroH.setText("");
        limpiarPaneles();
        numHex.setText("");
        rojo.setModel(new SpinnerNumberModel(0, 0, 255, 1));
        verde.setModel(new SpinnerNumberModel(0, 0, 255, 1));
        azul.setModel(new SpinnerNumberModel(0, 0, 255, 1));
        coloresM.setSelectedIndex(0);
        miColor=Color.WHITE;
        normal.setSelected(true);
    }
        
    }
    public static void main(String [] args){
         /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Proyecto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Proyecto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Proyecto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Proyecto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        Proyecto miProyecto= new Proyecto();
    }

}
