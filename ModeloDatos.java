

/************************************************************************************************************
 * Clase que controla el modelo de datos utilizados en la interfaz
 * Se utilizo el Modelo MVC para diseñar la estructura de la Interfaz( Modelo, Vista,Controlador)
 * Se utiliza un archivo llamado, Colores.csv contiene la información de los colores de muestra
 ************************************************************************************************************/
import java.awt.*;
import java.util.*;
public class ModeloDatos
{
    private ST<String, Color> st = new ST<String, Color>();
    private String nombreColores[];
    
    public ModeloDatos(){
        In in = new In("Colores.csv");
        int x=1;
        while (in.hasNextLine()) {
            String line = in.readLine();
            String[] datos = line.split(",");
            st.put(datos[0],new Color(hex(datos[1])));
        }
        nombreColores= new String[st.size()+1];
        nombreColores[0]="Seleccionar..";
        for (String s : st.keys()){
            nombreColores[x]=s;
            x++;
         }
        
    }
    public String[] getNombreColores(){
        return nombreColores;
    }
    public Color buscarColor(String nombreC){
        return st.get(nombreC);
    }
    public int hex(String s){
        System.out.println(s);
        String dat[]=s.split("#");
        return Integer.parseInt(dat[1],16);
    }
    public int[] getCantidadColores(Color c){
        int[] cantidad={c.getRed(),c.getGreen(),c.getBlue()};
    return cantidad;
    }
    private String hex(int num){
    int aux=(int)(Math.floor(num/16));
    int aux2=num%16;
    return ""+(aux==10?"A":((aux==11?"B":(aux==12?"C":(aux==13?"D":(aux==14?"E":(aux==15?"F":""+aux))))))) +
    (aux2==10?"A":((aux2==11?"B":(aux2==12?"C":(aux2==13?"D":(aux2==14?"E":(aux2==15?"F":""+aux2)))))));
    }
    public String getHexadecimal(Color c){
        String hex="#";
        hex=hex+hex(c.getRed())+hex(c.getGreen())+hex(c.getBlue());
        return hex;
    }
     public Color combine(Color c1, Color c2, double alpha) {
        int r = (int) (alpha * c1.getRed()   + (1 - alpha) * c2.getRed());
        int g = (int) (alpha * c1.getGreen() + (1 - alpha) * c2.getGreen());
        int b = (int) (alpha * c1.getBlue()  + (1 - alpha) * c2.getBlue());
        return new Color(r, g, b);
    }
    
    public String getCMYK(Color c){
        //The R,G,B values are divided by 255 to change the range from 0..255 to 0..1:
        //Se calcula el porcentaje de Cyan, Magenta, Yellow, Key(black)
        double rc=c.getRed()/255;
        double gc=c.getGreen()/255;
        double bc=c.getBlue()/255;
        double max=0;
        //The black key (K) color is calculated from the red (Rc), green (Gc) and blue (bc) colors:
        //K = 1-max(Rc, Gc, Bc)
            if(rc>gc && rc>bc) max=rc;
            else if(gc>rc && gc>bc) max=gc;
            else if(bc>rc && bc>gc) max=bc;
            else if(rc==gc && rc>bc) max=rc;
            else if(rc==bc && rc>gc) max=rc;
            else if(gc==bc && gc>rc) max=gc;
            else if(rc==gc && gc==bc) max=rc;
         //The cyan color (C) is calculated from the red (R') and black (K) colors:
         //Porcentajes de colores
        
         int k=(int)(1-max)*100;
         int cyan = (int)((1-rc-max) / (1-max))*100;
         //The magenta color (M) is calculated from the green (G') and black (K) colors
         int magenta=(int)((1-gc-max)/(1-max))*100;
         //The yellow color (Y) is calculated from the blue (B') and black (K) colors
         int yellow=(int)((1-bc-max)/(1-max))*100;
         
        String conversion="Cyan: "+cyan+"% Magenta: "+magenta+"% Yellow: "+yellow+"% Black Key: "+k+"%";
        System.out.println(conversion);
        return conversion;
    }
  
    
    
}
